#!/usr/bin/env python3
import asyncio
import os
import subprocess
from pathlib import Path

import requests
from orjson import OPT_APPEND_NEWLINE, OPT_INDENT_2, OPT_SORT_KEYS, loads
from repod.common.enums import FilesVersionEnum, PackageDescVersionEnum
from repod.repo import SyncDatabase

ORJSON_OPTION = OPT_INDENT_2 | OPT_APPEND_NEWLINE | OPT_SORT_KEYS

repos = ["core", "extra"]
package_base_blacklist = ["python-trio-websocket"]

imported_packages = []
for repo in repos:
    r = requests.get(f"https://geo.mirror.pkgbuild.com/{repo}/os/x86_64/{repo}.db")
    r.raise_for_status()
    with open(f"{repo}.db", "wb") as file:
        file.write(r.content)

    bases = []
    packages = []

    for base, outputpackagebase in asyncio.run(
        SyncDatabase(
            database=os.path.abspath(f"{repo}.db"),
            desc_version=PackageDescVersionEnum.DEFAULT,
            files_version=FilesVersionEnum.DEFAULT,
        ).outputpackagebases()
    ):
        bases.append(base)
        if base in package_base_blacklist:
            continue
        pkgbase_path = f"../x86_64/{repo}/{base}.json"
        missing_packages = []
        if Path(pkgbase_path).is_file():
            with open(pkgbase_path, "rb") as input_file:
                inputpackagebase = loads(input_file.read())
                packages.extend([package.get("name") for package in outputpackagebase.dict().get("packages")])
                if inputpackagebase.get("buildinfo") is not None and outputpackagebase.dict().get("version") == inputpackagebase.get("version"):
                    continue

        # https://gitlab.archlinux.org/archlinux/repod/-/issues/147
        if len(imported_packages) >= 500:
            continue

        filenames = []
        for package in outputpackagebase.dict().get("packages"):
            imported_packages.append(package)
            filename = package.get("filename")
            filenames.append(filename)
            url = f"https://geo.mirror.pkgbuild.com/{repo}/os/x86_64/{filename}"
            for suffix in ["", ".sig"]:
                r = requests.get(f"{url}{suffix}")
                r.raise_for_status()
                with open(f"{filename}{suffix}", "wb") as file:
                    file.write(r.content)
            print(f"Downloaded {filename}")
        filenames = [os.path.abspath(f"{filename}") for filename in filenames]
        env = os.environ.copy()
        env["XDG_STATE_HOME"] = os.getcwd()
        p = subprocess.run(
            ["pdm", "run", "repod-file", "-c", "../repod.conf", "repo", "importpkg", "--with-signature"] + filenames + [repo], capture_output=True, text=True, env=env
        )
        if p.returncode != 0:
            print(f"Failed importing pkgbase {base}, error: {p.stderr}")

    if os.path.isdir(f"repod/management/default/x86_64/{repo}"):
        for root, dirs, files in os.walk(f"repod/management/default/x86_64/{repo}", topdown=False):
            for name in dirs:
                dst = f"../x86_64/{repo}" + os.path.join(root, name).removeprefix(f"repod/management/default/x86_64/{repo}")
                os.makedirs(dst, exist_ok=True)
            for name in files:
                src = os.path.join(root, name)
                dst = f"../x86_64/{repo}" + src.removeprefix(f"repod/management/default/x86_64/{repo}")
                os.rename(src, dst)

    bases = set(bases)
    for base_file in os.listdir(f"../x86_64/{repo}"):
        if base_file != "pkgnames" and base_file.removesuffix(".json") not in bases:
            os.remove(f"../x86_64/{repo}/{base_file}")

    packages = set(packages)
    if os.path.isdir(f"../x86_64/{repo}/pkgnames"):
        for file in os.listdir(f"../x86_64/{repo}/pkgnames"):
            if file.removesuffix(".json") not in packages:
                os.remove(f"../x86_64/{repo}/pkgnames/{file}")
